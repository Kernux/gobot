package main

import (
	"encoding/json"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type music struct {
	Data []struct {
		ID                    int    `json:"id"`
		Readable              bool   `json:"readable"`
		Title                 string `json:"title"`
		TitleShort            string `json:"title_short"`
		TitleVersion          string `json:"title_version"`
		Link                  string `json:"link"`
		Duration              int    `json:"duration"`
		Rank                  int    `json:"rank"`
		ExplicitLyrics        bool   `json:"explicit_lyrics"`
		ExplicitContentLyrics int    `json:"explicit_content_lyrics"`
		ExplicitContentCover  int    `json:"explicit_content_cover"`
		Preview               string `json:"preview"`
		Artist                struct {
			ID            int    `json:"id"`
			Name          string `json:"name"`
			Link          string `json:"link"`
			Picture       string `json:"picture"`
			PictureSmall  string `json:"picture_small"`
			PictureMedium string `json:"picture_medium"`
			PictureBig    string `json:"picture_big"`
			PictureXl     string `json:"picture_xl"`
			Tracklist     string `json:"tracklist"`
			Type          string `json:"type"`
		} `json:"artist"`
		Album struct {
			ID          int    `json:"id"`
			Title       string `json:"title"`
			Cover       string `json:"cover"`
			CoverSmall  string `json:"cover_small"`
			CoverMedium string `json:"cover_medium"`
			CoverBig    string `json:"cover_big"`
			CoverXl     string `json:"cover_xl"`
			Tracklist   string `json:"tracklist"`
			Type        string `json:"type"`
		} `json:"album"`
		Type string `json:"type"`
	} `json:"data"`
	Total int    `json:"total"`
	Next  string `json:"next"`
}

type news struct {
	Status       string `json:"status"`
	TotalResults int    `json:"totalResults"`
	Articles     []struct {
		Source struct {
			ID   string `json:"id"`
			Name string `json:"name"`
		} `json:"source"`
		Author      string    `json:"author"`
		Title       string    `json:"title"`
		Description string    `json:"description"`
		URL         string    `json:"url"`
		URLToImage  string    `json:"urlToImage"`
		PublishedAt time.Time `json:"publishedAt"`
		Content     string    `json:"content"`
	} `json:"articles"`
}
type chuck struct {
	Categories []interface{} `json:"categories"`
	CreatedAt  string        `json:"created_at"`
	IconURL    string        `json:"icon_url"`
	ID         string        `json:"id"`
	UpdatedAt  string        `json:"updated_at"`
	URL        string        `json:"url"`
	Value      string        `json:"value"`
}
type unsplash struct {
	ID             string      `json:"id"`
	CreatedAt      string      `json:"created_at"`
	UpdatedAt      string      `json:"updated_at"`
	Width          int         `json:"width"`
	Height         int         `json:"height"`
	Color          string      `json:"color"`
	Description    interface{} `json:"description"`
	AltDescription interface{} `json:"alt_description"`
	Urls           struct {
		Raw     string `json:"raw"`
		Full    string `json:"full"`
		Regular string `json:"regular"`
		Small   string `json:"small"`
		Thumb   string `json:"thumb"`
	} `json:"urls"`
	Links struct {
		Self             string `json:"self"`
		HTML             string `json:"html"`
		Download         string `json:"download"`
		DownloadLocation string `json:"download_location"`
	} `json:"links"`
	Categories             []interface{} `json:"categories"`
	Sponsored              bool          `json:"sponsored"`
	SponsoredBy            interface{}   `json:"sponsored_by"`
	SponsoredImpressionsID interface{}   `json:"sponsored_impressions_id"`
	Likes                  int           `json:"likes"`
	LikedByUser            bool          `json:"liked_by_user"`
	CurrentUserCollections []interface{} `json:"current_user_collections"`
	User                   struct {
		ID              string      `json:"id"`
		UpdatedAt       string      `json:"updated_at"`
		Username        string      `json:"username"`
		Name            string      `json:"name"`
		FirstName       string      `json:"first_name"`
		LastName        string      `json:"last_name"`
		TwitterUsername interface{} `json:"twitter_username"`
		PortfolioURL    interface{} `json:"portfolio_url"`
		Bio             string      `json:"bio"`
		Location        string      `json:"location"`
		Links           struct {
			Self      string `json:"self"`
			HTML      string `json:"html"`
			Photos    string `json:"photos"`
			Likes     string `json:"likes"`
			Portfolio string `json:"portfolio"`
			Following string `json:"following"`
			Followers string `json:"followers"`
		} `json:"links"`
		ProfileImage struct {
			Small  string `json:"small"`
			Medium string `json:"medium"`
			Large  string `json:"large"`
		} `json:"profile_image"`
		InstagramUsername string `json:"instagram_username"`
		TotalCollections  int    `json:"total_collections"`
		TotalLikes        int    `json:"total_likes"`
		TotalPhotos       int    `json:"total_photos"`
		AcceptedTos       bool   `json:"accepted_tos"`
	} `json:"user"`
	Exif struct {
		Make         string `json:"make"`
		Model        string `json:"model"`
		ExposureTime string `json:"exposure_time"`
		Aperture     string `json:"aperture"`
		FocalLength  string `json:"focal_length"`
		Iso          int    `json:"iso"`
	} `json:"exif"`
	Views     int `json:"views"`
	Downloads int `json:"downloads"`
}
type kek struct {
	Ok     bool `json:"ok"`
	Result struct {
		User struct {
			ID           int    `json:"id"`
			IsBot        bool   `json:"is_bot"`
			FirstName    string `json:"first_name"`
			Username     string `json:"username"`
			LanguageCode string `json:"language_code"`
		} `json:"user"`
		Status string `json:"status"`
	} `json:"result"`
}

var bh [100]string

//func wordCheck(text string, userID int, chatID int64) {
//	for j := 0; j < len(bh); j++ {
//		if strings.Contains(text, bh[j]) {
//			kick(userID, chatID)
//		}
//	}
//}
//TODO REFACTOR
func main() {
	elements := map[string]bool{
		"rust":             true,
		"echo":             true,
		"suicide":          false,
		"pyhton":           true,
		"help":             false,
		"ban":              true,
		"bh":               true,
		"ping":             true,
		"savestab":         false,
		"f":                true,
		"gay":              true,
		"8":                true,
		"bruh":             true,
		"unsplash":         true,
		"pycharm":          false,
		"suck":             false,
		"music":            false,
		"flex":             true,
		"shrug":            true,
		"fix":              false,
		"Foxed":            false,
		"chuck":            true,
		"news":             true,
		"shrek":            true,
		"random":           true,
		"ship":             true,
		"fuck":             false,
		"stable":           false,
		"productplacement": false,
		"info":             true,
		"changelog":        true,
		"allcomands":		true,
		"zone":				true,
		"mute":             true,
		"reactions":		true,
	}
	var (
		unsplashResponse = "https://api.unsplash.com/photos/random?client_id=1435c8eaadfbeacd502ec854e73123059456f3a601722e790c009bd40fdfe15b"
		chuckResponse    = "https://api.chucknorris.io/jokes/random"
		newsResponse     = "https://newsapi.org/v2/top-headlines?country=ru&apiKey=4ae2630c606c46bb99756be01d9bb174"
	)
	var (
		text string
	)
	var (
		random    int
		counter   int
		strrandom string
	)
	var (
		replyID int
	)
	var (
		chatID    int64
		strchatID string
	)
	var (
		userID    int
		struserID string
	)
	var (
		stableID    int
		strstableID string
	)
	var mTime string
	var userIDs []int
	var changelog [10]string
	var bh [100]string
	changelog[1] = "[+]changelog[alpha]\n [-] old pyhton(yes,it's pyhton)\n [+]banhammer[alpha](I do not know why this work(лютый *****код))\n [=]Небольшой рефакторинг(нужно было для введения банахммера)\n [=]Разграничение каждого кейса на потоки + забивание новыми :DDDDD\n[=]Trash remooved\n[=]Old git commit :|\n"
	changelog[5] = "[+]changelog v0.2\n[+]Почва для новых обновлений\n[+]Bruh\n[+]Начал пилить сайт на https://ru.000webhost.com, но что-то как-то веб, и я как-то запутался в этом js и php, короче теперь сервер перегружен фреймворками и ненужным стафом,поэтому бот работает медленнеe\n[=]Багфиксы\n[+]Интеграция с GoMath(тестовый режим, pre-alpha(нет на битбакете, ибо под это дело надо создать отдельную ветку, а мне пока не до этого)\n[+]Shrek(Shrek is love, Shrek is life)(TTS will be after)(Idea by @CirnoIsNotBaka)\n[=]Bugfixes\n[-]Warnings\nP.S\n Начал делать бд, но оказывается Data-Science сложная штука, и ей надо учиться, ибо бд не в MySql пилятся труднее чем я думал, возможно это затянется. Пока никаких нормальных мыслей по добавлению функций нет, добавляю по запросу @denizvukolov"
	changelog[6] = "[+]ADMIN PANEL(наверное самое лютое нововведение за последний месяц, теперь админы могут полностью менеджиить бота под нужны конкретной группы или паблика(позже сделаю бд с индивидуальными натсройками для каждого чата))\n[+]Интеграция с GoMath\n[+]я криворукий дебил, поэтому в тестовом режиме работает расписание МАИ[для людей с айфонами временное решение, пока я не запилил нормальное приложение)\n[+]Почва для новых обновлений\n[+]Готова бд:)\n[+]Учу Django, так что скоро будет сайт, скорее всего будет схоронен там же где и бд:https://ru.000webhost.com\n[+]Несколько TODO элементов\n[+]комментарии в коде для тех, кто решит доработать бота или сделать своего /shrug(скоро нормально задокументирую\n[=]Багфиксы\n[=]Почти закончил Рефакторинг"
	changelog[7] = "[+]СЕРВЕР В НИДЕРЛАНДАХ. УЛЬТРАМОЩНЫЙ ОДНОЯДЕРНЫЙ XEON с 4 ГЕКТАРАМИ ОПЕРАТИВЫ\n[+]Ship. Теперь можно шиперить людей в чатах. Участникам достаточно зарегеистроваться, а админы должны будут запустить(/shipreg,/shipstart)\n[+]Fuck(@denizvukolov)\n[=]Почва для новых обновлений\n[=]Багфиксы\n[=]ФИКС ШРЕКА"
	var commandList [30]string
	commandList[0] = "pyhton"
	commandList[1] = "help"
	commandList[2] = "Maybe later..."
	//TODO new token
	//858109721:AAFLuKz-S0XA-6Yv5IhMOa6jbePlCGbyhYE - Luliks
	//669872325:AAFU0Fn6QHXnoU12LYi7CxxXem2GF8eemDA -Kek
	var token = "669872325:AAFU0Fn6QHXnoU12LYi7CxxXem2GF8eemDA"
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}
	bot.Debug = true
	log.Printf("Connection complete %s", bot.Self.UserName)
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)
	//TODO new msg generate algorithm
	for update := range updates {
		if update.Message == nil {
			continue
		}
		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
		// if !update.Message.IsCommand() {
		// 	 if update.Message.Text != "" {
		// 	 	userID = update.Message.From.ID
		// 	 	chatID = update.Message.Chat.ID
		// 	 	go wordCheck(update.Message.Text, userID, chatID)
		// 	}
		//TODO normal threads
		// }
		if update.Message.IsCommand() != true{
			if elements["reactions"] == true{
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")
			msgText := update.Message.Text
			chatID = update.Message.Chat.ID
			strchatID = strconv.FormatInt(chatID, 10)
			//AAQEAAPfAQAC_I0lUPxpNhzUzXCZmxazGwAEAQAHbQADqiIAAhYE
			//CgADBAAD0wEAAv8wJVB9DE8tA9F2vBYE - Aliance
			if strings.Contains(msgText, "wow") == true || strings.Contains(msgText, "вов") == true || strings.Contains(msgText, "Вов") || strings.Contains(msgText, "Варкарфт") {
				msg.Text = "ЗА АЛЬЯНС"
				http.Get("https://api.telegram.org/bot" + token + "/sendAnimation?chat_id=" + strchatID + "&animation=CgADBAAD0wEAAv8wJVB9DE8tA9F2vBYE")
				bot.Send(msg)
			}
			//CgADBAAD3wEAAvyNJVD8aTYc1M1wmRYE - orks
			if strings.Contains(msgText, "орду") == true || strings.Contains(msgText, "орк") == true || strings.Contains(msgText, "орки") || strings.Contains(msgText, "орда") {
				msg.Text = "Прятаться - это не про нас"
				http.Get("https://api.telegram.org/bot" + token + "/sendAnimation?chat_id=" + strchatID + "&animation=CgADBAAD3wEAAvyNJVD8aTYc1M1wmRYE")
				bot.Send(msg)
		}
			if strings.Contains(msgText, "педик") == true || strings.Contains(msgText, "пидор") == true || strings.Contains(msgText, "Пидор") || strings.Contains(msgText, "гей") {
				msg.Text = "Сам пидор"
				bot.Send(msg)
			}
			if strings.Contains(msgText, "Справедливо") || strings.Contains(msgText, "справедливо"){
				//CAADAgADVAADuRtZC2dCUqTSakgJFgQ
				http.Get("https://api.telegram.org/bot" + token + "/sendSticker?chat_id=" + strchatID + "&sticker=CAADAgADVAADuRtZC2dCUqTSakgJFgQ")
				// bot.Send(msg)
			}
			if strings.Contains(msgText, "Нисан") || strings.Contains(msgText, "нисан")|| strings.Contains(msgText, "Муррано") || strings.Contains(msgText, "муррано")|| strings.Contains(msgText, "думгай") || strings.Contains(msgText, "Ниссан") || strings.Contains(msgText, "ниссан"){
				if update.Message.Chat.ID != -1001281362497{
					//CAADAgADPgAD3KYZGImHPhNltsOMFgQ
					msg.Text = "Прокатить?"
					http.Get("https://api.telegram.org/bot" + token + "/sendSticker?chat_id=" + strchatID + "&sticker=CAADAgADPgAD3KYZGImHPhNltsOMFgQ")
					bot.Send(msg)
				}
			}
			if strings.Contains(msgText, "да") || strings.Contains(msgText, "Да"){
				msg.Text = "Пизда"
				bot.Send(msg)
			}
		}
	}
		if update.Message.IsCommand() {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")
			switch update.Message.Command() {
			case "rust":
				if elements["rust"] == true {
					msg.Text = "Love ❤️\nhttps://www.rust-lang.org/"
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "echo":
				if elements["echo"] == true {
					text = update.Message.Text
					//var echoText []string
					s := strings.Split(text, " ")
					if len(s) >= 2 {
						text2 := strings.Replace(text, "/echo", " ", -1)
						msg.Text = text2
					} else {
						msg.Text = "Fuck you"
					}
				}
			case "suicide":
				if elements["suicide"] == true {
					chatID = update.Message.Chat.ID
					userID = update.Message.From.ID
					struserID = strconv.Itoa(userID)
					strchatID = strconv.FormatInt(chatID, 10)
					_, _ = http.Get("https://api.telegram.org/bot" + token + "/kickChatMember?chat_id=" + strchatID + "&user_id=" + struserID)
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "pyhton":
				if elements["pyhton"] == true {
					msg.Text = "I hate this"
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "help":
				if elements["help"] == true {
					for i := range commandList {
						msg.Text += commandList[i] + "\n"
					}
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "ban":
				if elements["ban"] == true {
					if update.Message.ReplyToMessage != nil {
						chatID = update.Message.Chat.ID
						userID = update.Message.From.ID
						if checkAdmin(&strchatID, &struserID, &chatID, &userID) == true {
							log.Printf("true")
							replyID = update.Message.ReplyToMessage.From.ID
							kick(replyID, chatID)
						} else {
							msg.Text = "I fucked up"
						}
					} else {
						msg.Text = "No no no, please reply, and only after - try ban"
					}
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "bhadd":
				if elements["bh"] == true {
					userID = update.Message.From.ID
					chatID = update.Message.Chat.ID
					if checkAdmin(&strchatID, &struserID, &chatID, &userID) == true {
						s := strings.Split(update.Message.Text, " ")
						if len(s) == 2 {
							for i := 0; i < len(bh); i++ {
								log.Print(s)
								bh[i] = s[1]
								if bh[i] == "" || bh[i] == " " {
									break
								}
							}
						} else {
							msg.Text = "Mistake, not enough arguments"
						}
					} else {
						msg.Text = "Are you sure?(Not admin)"
					}
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "bhlist":
				if elements["bh"] == true {
					for i := 0; i < len(bh); i++ {
						msg.Text = msg.Text + "\n" + bh[i]
					}
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "ping":
				if elements["ping"] == true {
					message := tgbotapi.NewMessage(update.Message.Chat.ID, "Fuck you")
					_, _ = bot.Send(message)
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "savestab":
				if elements["savestab"] == true {
					stableID = update.Message.MessageID
					chatID = update.Message.Chat.ID
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "f":
				if elements["f"] == true {
					chatID = update.Message.Chat.ID
					strchatID = strconv.FormatInt(chatID, 10)
					_, _ = http.Get("https://api.telegram.org/bot" + token + "/sendSticker?chat_id=" + strchatID + "&sticker=CAADAgADsgADTptkAm1WnTBWvUfiAg")
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "gay":
				if elements["gay"] == true {
					random = rand.Intn(100)
					msg.Text = "You are gay with chance:" + strconv.Itoa(random) + "%"
					counter++
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "8": //TODO new random algorithm(maybe write custom?)
				if elements["8"] == true {
					random = rand.Intn(6)
					switch random {
					case 0:
						msg.Text = "Мой ответ - 'да'"
					case 1:
						msg.Text = "Скорее всего да"
					case 2:
						msg.Text = "хз"
					case 3:
						msg.Text = "Скорее всего нет"
					case 4:
						msg.Text = "Давай ещё раз"
					case 5:
						msg.Text = "Мой ответ-'нет'"
					}
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "bruh":
				if elements["bruh"] == true {
					bruhText := strings.Split(update.Message.Text, " ")
					log.Print(bruhText)
					log.Print(len(bruhText))
					if len(bruhText) >= 2 {
						bruhNumb, err := strconv.Atoi(bruhText[1])
						errcheck(&err)
						if bruhNumb < 0 || bruhNumb > 10000 {
							msg.Text = "What? Mistake\n"
						} else {
							msg.Text = "br" + strings.Repeat("u", bruhNumb) + "h" + "🤢"
						}
					}
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "unsplash":
				if elements["unsplash"] == true {
					userID = update.Message.From.ID
					if userID == 847529348 {
						msg.Text = "Suck"
					} else {
						image, er := os.Create("image.jpeg")
						errcheck(&er)
						httpGet, err := http.Get(unsplashResponse)
						errcheck(&err)
						var photos = unsplash{}
						_ = json.NewDecoder(httpGet.Body).Decode(&photos)
						log.Print(photos.Links.Download)
						out, err := http.Get(photos.Links.Download)
						errcheck(&err)
						_, _ = io.Copy(image, out.Body)
						// file, verr := os.Open(image.Name())
						// errcheck(&verr)
						http.Get("https://api.telegram.org/bot" + token + "/sendPhoto?chat_id=404334300&photo=/home/rob/image.jpeg")
						msg.Text = photos.Links.Download
					}
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "pycharm":
				if elements["pycharm"] == true {
					msg.Text = "Fuck you"
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "suck":
				if elements["suck"] == true {
					msg.Text = "Suck"
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "music":
				if elements["music"] == true {
					random = rand.Intn(300000-100000) + 100000
					httpGet, err := http.Get("https://api.deezer.com/search?q=queen")
					mp3, er := os.Create("music.mp3")
					errcheck(&er)
					errcheck(&err)
					var mus = music{}
					_ = json.NewDecoder(httpGet.Body).Decode(&mus)
					log.Print(mus.Total)
					out, ver := http.Get(mus.Data[1].Link)
					errcheck(&ver)
					_, _ = io.Copy(mp3, out.Body)
					chatID = update.Message.Chat.ID
					strchatID = strconv.FormatInt(chatID, 10)
					_, _ = http.Get("https://api.telegram.org/bot" + token + "/sendAudio?chat_id=404334300&audio=music.mp3")
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "flex":
				if elements["flex"] == true {
					chatID = update.Message.Chat.ID
					strchatID = strconv.FormatInt(chatID, 10)
					_, _ = http.Get("https://api.telegram.org/bot" + token + "/sendAnimation?chat_id=" + strchatID + "&animation=CgADAgADLQMAAn-E6UlWs6GdWI1ZvgI")
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "shrug":
				if elements["shrug"] == true {
					msg.Text = "¯\\_(ツ)_/¯"
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "fix": //// TODO: delete this
				if elements["fix"] == true {
					if update.Message.ReplyToMessage != nil {
						replyID = update.Message.ReplyToMessage.From.ID
						msg.Text = strconv.Itoa(replyID)
					}
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "Foxed":
				if elements["Foxed"] == true {
					msg.Text = "http://qiwi.me/f0x1d"
				}
			case "chuck":
				if elements["chuck"] == true {
					httpGet, err := http.Get(chuckResponse)
					errcheck(&err)
					var Chuck = chuck{}
					_ = json.NewDecoder(httpGet.Body).Decode(&Chuck)
					text = Chuck.Value
					msg.Text = text
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "news":
				if elements["news"] {
					random = rand.Intn(7)
					httpGet, err := http.Get(newsResponse)
					errcheck(&err)
					var news = news{}
					json.NewDecoder(httpGet.Body).Decode(&news)
					msg.Text = news.Articles[random].Title + "\n" + "\n" + news.Articles[random].URL + "\n"
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "shrek":
				if elements["shrek"] {
					strchatID = strconv.FormatInt(update.Message.Chat.ID, 10)
					//CgADBAADrwEAAsZPzVNkLxmZW353-xYE - shrek hello
					//CgADBAADrwEAAsZPzVNkLxmZW353-xYE - repeat?
					http.Get("https://api.telegram.org/bot" + token + "/sendAnimation?chat_id=" + strchatID + "&animation=CgADBAADrwEAAsZPzVOlT5mtSHBQrRYE")
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "random":
				if elements["random"] == true {
					random = rand.Intn(32769)
					strrandom = strconv.Itoa(random)
					msg.Text = strrandom
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "shipreg":
				if elements["ship"] {
					userIDs = append(userIDs, update.Message.From.ID)
					log.Print(userIDs)
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "shipstart":
				if elements["ship"] {
					chatID = update.Message.Chat.ID
					userID = update.Message.From.ID
					strchatID = strconv.FormatInt(chatID, 10)
					struserID = strconv.Itoa(userID)
					if checkAdmin(&strchatID, &struserID, &chatID, &userID) {
						struserID = strconv.Itoa(userIDs[rand.Intn(len(userIDs))])
						secondStrUserID := strconv.Itoa(userIDs[rand.Intn(len(userIDs))])
						msg.Text = "Море волнуется раз..."
						bot.Send(msg)
						time.Sleep(2 * time.Second)
						msg.Text = "Море волнуется два..."
						bot.Send(msg)
						time.Sleep(2 * time.Second)
						msg.Text = "Море волнуется три..."
						bot.Send(msg)
						time.Sleep(4 * time.Second)
						apiGet, err := http.Get("https://api.telegram.org/bot" + token + "/getChatMember?chat_id=" + strchatID + "&user_id=" + struserID)
						errcheck(&err)
						var app = kek{}
						json.NewDecoder(apiGet.Body).Decode(&app)
						apiGet2, err := http.Get("https://api.telegram.org/bot" + token + "/getChatMember?chat_id=" + strchatID + "&user_id=" + secondStrUserID)
						errcheck(&err)
						var app2 = kek{}
						json.NewDecoder(apiGet2.Body).Decode(&app2)
						msg.Text = "Сливаются узы любви:\n" + "@" + app.Result.User.FirstName + " 💖 " + "@" + app2.Result.User.FirstName
					} else {
						msg.Text = "Na"
					}
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "shipclear":
				if elements["ship"] {
					chatID = update.Message.Chat.ID
					userID = update.Message.From.ID
					strchatID = strconv.FormatInt(chatID, 10)
					struserID = strconv.Itoa(userID)
					if checkAdmin(&strchatID, &struserID, &chatID, &userID) {
						for i := 0; i < len(userIDs); i++ {
							userIDs[i] = 0
						}
					}
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "fuck":
				if elements["fuck"] {
					msg.Text = "Fuck you leatherman"
					strchatID = strconv.FormatInt(update.Message.Chat.ID, 10)
					//CgADAgADQQIAAqhxYUqCa2FT3t9uNhYE-gachi
					http.Get("https://api.telegram.org/bot" + token + "/sendAnimation?chat_id=" + strchatID + "&animation=CgADAgADQQIAAqhxYUqCa2FT3t9uNhYE")
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "stable":
				if elements["stable"] == true {
					strstableID = strconv.Itoa(stableID)
					chatID = update.Message.Chat.ID
					strchatID = strconv.FormatInt(chatID, 10)
					chatID2 := update.Message.Chat.ID
					strchatID2 := strconv.FormatInt(chatID2, 10)
					http.Get("https://api.telegram.org/bot" + token + "/forwardMessage?chat_id=" + strchatID2 + "&from_chat_id=" + strchatID + "&message_id=" + strstableID)
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "productplacement":
				if elements["productplacement"] == true {
					msg.Text = "Привет, сегодня днем тут в чате у меня спрашивали про инстересную тему, которую я нашел, вот ссылка на нее - @Kernux(ссылка в ЛС)\n\nГлавное понять правильно как использовать выгодно инфу что там есть, у меня получилось ну очень прибыльно!)"
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "info":
				if elements["info"] == true {
					msg.Text = "Author:@Kernux\n Server:@murl33k\nHello World:Hello,World!"
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "changelog":
				//TODO recode with array(mb jeneric?)
				if elements["changelog"] == true {
					s := strings.Split(update.Message.Text, " ")
					if len(s) >= 2 {
						if s[1] == "old" || s[1] == "Old" {
							msg.Text = changelog[1]
						} else if s[1] == "minor" || s[1] == "Minor" {
							msg.Text = changelog[7]
						} else {
							msg.Text = changelog[6]
							// } else {
							// 	var changelogNumb int
							// 	changelogNumb, err = strconv.Atoi(s[1])
							// 	errcheck(&err)
							// 	if changelogNumb <= len(changelog) && changelogNumb > 0 {
							// 		msg.Text = changelog[changelogNumb]
							//}
							// 	}
						}
					}
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "allcomands":
				if elements["allcomands"] == true{
					msg.Text = "/rust\n/echo\n/suicide\n/pyhton\n/help\n/ban\n/bh\n/ping\n/savestab\n/f\n/gay\n/8\n/bruh\n/unsplash\n/pycharm\n/suck\n/music\n/flex\n/shrug\n/fix\n/Foxed\n/chuck\n/news\n/shrek\n/random\n/ship\n/fuck\n/stable\n/productplacement\n/info\n/changelog\n/allcomands\n/zone\n/mute\n"
				}else{
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "zone":
				if elements["zone"] == true{
					random = rand.Intn(10)
					switch(random){
				case 0:
					msg.Text = "Ты на кого батон крошишь?"
				case 1:
					msg.Text = "Твоё место у параши"
				case 2:
					msg.Text = "Съебались все по мастям по областям."
				case 3:
					msg.Text = "Базарят бабки на базаре мы по жизни речь толкуем!"
				case 4:
					msg.Text = "Еду, буду, ждите днями, прячте всё и прячтесь сами!"
				case 5:
					msg.Text = "Вору дай хоть миллион он воровать не перестанет."
				case 6:
					msg.Text = "Розы гибнут на морозе, а пацаны при передозе"
				case 7:
					msg.Text = "Будешь ночью много спать, перестанешь воровать!"
				case 8:
					msg.Text = "Суд - это базар, где торгуют свободой не зная её цены!"
				case 9:
					msg.Text = "Ты че - крысу во мне увидил, укачало по ходу!"
				case 10:
					msg.Text = "Ты кто по масти конь дурной - я в зубы дам тебе трубой."
				}
			}else{
					msg.Text = "Disabled by Grand Master @Kernux"
				}

			case "mute":
				if elements["mute"] == true {
					chatID = update.Message.Chat.ID
					strchatID = strconv.FormatInt(chatID, 10)
					if update.Message.ReplyToMessage != nil {
						userID = update.Message.ReplyToMessage.From.ID
						adminId := update.Message.From.ID
						struserID = strconv.Itoa(userID)
						if checkAdmin(&strchatID, &struserID, &chatID, &adminId) {
							s := strings.Split(update.Message.Text, " ")
							if len(s) > 1 {
							}
						} else {
							msg.Text = "Na"
						}
						if len(mTime) >= 1 {
							chatID = update.Message.Chat.ID
							strchatID = strconv.FormatInt(chatID, 10)
							userID = update.Message.ReplyToMessage.From.ID
							struserID = strconv.Itoa(userID)
							intMtime, err := strconv.ParseInt(mTime, 10, 64)
							errcheck(&err)
							muteTime := time.Now().UTC().Unix() + 60*intMtime
							strMuteTime := strconv.FormatInt(muteTime, 10)
							http.Get("https://api.telegram.org/bot" + token + "/restrictChatMember?chat_id=" + strchatID + "&user_id=" + struserID + "&permissions{can_send_messages:false,until_date:" + strMuteTime + "}")
							// struserID = strconv.Itoa(userID)
							// a := tgbotapi.ChatMemberConfig{chatID, strchatID, strchatID, userID}
							// errcheck(&err)
							// b := tgbotapi.RestrictChatMemberConfig{a, strconv.ParseFloat(time.October.String), nil, nil, nil}
							// bot.RestrictChatMember(b)
						}
					} else {
						msg.Text = "Error,not enough arguments"
					}
				} else {
					msg.Text = "Disabled by Grand Master @Kernux"
				}
			case "promote":
				chatID = update.Message.Chat.ID
				strchatID = strconv.FormatInt(chatID, 10)
				userID = update.Message.From.ID
				struserID = strconv.Itoa(userID)
				if checkAdmin(&strchatID, &struserID, &chatID, &userID) {
					if update.Message.ReplyToMessage != nil {
						var url = "TODO"
						http.Get(url)
					}
				}
			case "admin":
				chatID = update.Message.Chat.ID
				strchatID = strconv.FormatInt(chatID, 10)
				userID = update.Message.From.ID
				struserID = strconv.Itoa(userID)
				if checkAdmin(&strchatID, &struserID, &chatID, &userID) {
					s := strings.Split(update.Message.Text, " ")
					if len(s) < 3 {
						msg.Text = "Not enough arguments in call /admin"
					} else {
						for key, value := range elements {
							if s[2] == key {
								if s[1] == "enable" || s[1] == "Enable" {
									if value == true {
										msg.Text = "Skynet's a machine, and like all machines it has an off switch"
									} else {
										elements[key] = true
										msg.Text = "Enabled\n\nDetective Del Spooner: Can a robot write a symphony? Can a robot take a blank canvas and turn it into a masterpiece?\nSonny: Can you?"
									}
								} else if s[1] == "disable" || s[1] == "Disable" {
									if value == true {
										elements[key] = false
										msg.Text = "A robot may not harm humanity, or, by inaction, allow humanity to come to harm. Or not?"
									} else {
										msg.Text = "Already deactivated\n\nWe are robots. We don't do coincidence"
									}
								}
							}
						}
					}
				} else {
					msg.Text = "Only the Chosen One by The Oracles of The Universe can defeat him. The darkest evil in the Galaxy"
				}
				// 	if s[1] == "enable" || s[1] == "Enable" {
				// 		if isActive {
				// 			msg.Text = "Skynet's a machine, and like all machines it has an off switch"
				// 		} else {
				// 			isActive = true
				// 			msg.Text = "Enabled\n\nDetective Del Spooner: Can a robot write a symphony? Can a robot take a blank canvas and turn it into a masterpiece?\nSonny: Can you?"
				// 		}
				// 	} else if s[1] == "disable" || s[1] == "Disable" {
				// 		if isActive == true {
				// 			isActive = false
				// 			msg.Text = "A robot may not harm humanity, or, by inaction, allow humanity to come to harm. Or not?"
				// 		} else {
				// 			msg.Text = "Already deactivated\n\nWe are robots. We don't do coincidence"
				// 		}
				// 	}
				// }

			default:
				//msg.Text = "Correct your command please"
			}
			bot.Send(msg)
		}
	}
}
