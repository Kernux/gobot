# Welcome to goBot 👋
![Version](https://img.shields.io/badge/version-0.8.2-blue.svg?cacheSeconds=2592000)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)](goDoc)
[![License: GPL 3.0](https://img.shields.io/badge/License-GPL 3.0-yellow.svg)](https://www.gnu.org/licenses/gpl-3.0.ru.html)
[![Twitter: GrobKern](https://img.shields.io/twitter/follow/GrobKern.svg?style=social)](https://twitter.com/GrobKern)

> Test.Nothing more

### 🏠 [Homepage](https://bitbucket.org/Kernux/gobot/src/master/)

### ✨ [Demo](t.me/Kekalbot)

## Install

```sh
git clone https://Kernux@bitbucket.org/Kernux/gobot.git
```

## Usage

```sh
/help
```

## Run tests

```sh
/ping
```

## Author

👤 **Kernux**

* Twitter: [@GrobKern](https://twitter.com/GrobKern)
* Github: [@Kernux](https://github.com/Kernux)

## Show your support

Give a ⭐️ if this project helped you!


## 📝 License

Copyright © 2019 [Kernux](https://github.com/Kernux).

This project is [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.ru.html) licensed.

***
